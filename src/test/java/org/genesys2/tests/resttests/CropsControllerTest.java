package org.genesys2.tests.resttests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CropsControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(CropsControllerTest.class);

    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    private Crop crop;
    private CropRule cropRule;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        crop = cropService.addCrop("ShortName crop", "Name Crop", "description crop", "en");

        cropRule = cropService.addCropRule(crop, "0", "species", true);

        List<CropRule> cropRules = new ArrayList<>();
        cropRules.add(cropRule);
        crop.setCropRules(cropRules);
    }

    @After
    public void tearDown() {
        cropRepository.deleteAll();
    }

    @Test
    public void listCropsTest() throws Exception {
        LOG.info("Start test-method listCropsTest");

        mockMvc.perform(get("/api/v0/crops")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].version", is(0)))
                .andExpect(jsonPath("$[0].shortName", is("ShortName crop")))
                .andExpect(jsonPath("$[0].i18n", is("en")))
                .andExpect(jsonPath("$[0].name", is("Name Crop")))
                .andExpect(jsonPath("$[0].description", is("description crop")));

        LOG.info("Test listCropsTest passed");
    }

    @Test
    public void createCropTest() throws Exception {
        LOG.info("Start test-method createCropTest");

        cropRepository.deleteAll();

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/v0/crops")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(crop)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.version", is(0)))
                .andExpect(jsonPath("$.shortName", is("ShortName crop")))
                .andExpect(jsonPath("$.i18n", is("en")))
                .andExpect(jsonPath("$.name", is("Name Crop")))
                .andExpect(jsonPath("$.description", is("description crop")));

        LOG.info("Test createCropTest passed");
    }

    @Test
    public void getCropTest() throws Exception {
        LOG.info("Start test-method getCropTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(notNullValue())))
                .andExpect(jsonPath("$.version", is(0)))
                .andExpect(jsonPath("$.shortName", is("ShortName crop")))
                .andExpect(jsonPath("$.i18n", is("en")))
                .andExpect(jsonPath("$.name", is("Name Crop")))
                .andExpect(jsonPath("$.description", is("description crop")));

        LOG.info("Test getCropTest passed");
    }

    @Test
    public void deleteCropTest() throws Exception {
        LOG.info("Start test-method deleteCropTest");

        mockMvc.perform(delete("/api/v0/crops/{shortName}", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(nullValue())))
                .andExpect(jsonPath("$.version", is(0)))
                .andExpect(jsonPath("$.shortName", is("ShortName crop")))
                .andExpect(jsonPath("$.i18n", is("en")))
                .andExpect(jsonPath("$.name", is("Name Crop")))
                .andExpect(jsonPath("$.description", is("description crop")));

        LOG.info("Test deleteCropTest passed");
    }

    @Test
    public void getCropDescriptorsTest() throws Exception {
        LOG.info("Start test-method getCropDescriptorsTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}/descriptors", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(0)))
                .andExpect(jsonPath("$.lastPage", is(true)))
                .andExpect(jsonPath("$.numberOfElements", is(0)))
                .andExpect(jsonPath("$.totalElements", is(0)))
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(jsonPath("$.firstPage", is(true)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.totalPages", is(0)))
                .andExpect(jsonPath("$.size", is(50)));

        LOG.info("Test getCropDescriptorsTest passed");
    }

    @Test
    public void getCropRulesTest() throws Exception {
        LOG.info("Start test-method getCropRulesTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}/rules", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].included", is(true)))
                .andExpect(jsonPath("$[0].genus", is("0")))
                .andExpect(jsonPath("$[0].species", is("species")))
                .andExpect(jsonPath("$[0].subtaxa", is(nullValue())));

        LOG.info("Test getCropRulesTest passed");
    }

    @Test
    public void updateCropRulesTest() throws Exception {
        LOG.info("Start test-method updateCropRulesTest");

        ObjectMapper objectMapper = new ObjectMapper();
        List<CropRule> cropRules = new ArrayList<>();
        cropRules.add(cropRule);

        mockMvc.perform(put("/api/v0/crops/{shortName}/rules", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cropRules)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(notNullValue())))
                .andExpect(jsonPath("$[0].included", is(true)))
                .andExpect(jsonPath("$[0].genus", is("0")))
                .andExpect(jsonPath("$[0].species", is("species")))
                .andExpect(jsonPath("$[0].subtaxa", is(nullValue())));

        LOG.info("Test updateCropRulesTest passed");
    }

    @Test
    public void getCropTaxaTest() throws Exception {
        LOG.info("Start test-method getCropTaxaTest");

        mockMvc.perform(get("/api/v0/crops/{shortName}/taxa", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content", hasSize(0)))
                .andExpect(jsonPath("$.lastPage", is(true)))
                .andExpect(jsonPath("$.numberOfElements", is(0)))
                .andExpect(jsonPath("$.totalElements", is(0)))
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(jsonPath("$.firstPage", is(true)))
                .andExpect(jsonPath("$.first", is(true)))
                .andExpect(jsonPath("$.totalPages", is(0)))
                .andExpect(jsonPath("$.size", is(50)));

        LOG.info("Test getCropTaxaTest passed");
    }

    @Test
    public void rebuildTest() throws Exception {
        LOG.info("Start test-method rebuildTest");

        String JSON_OK = "{\"result\":true}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/crops/rebuild")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(jsonNode)));

        LOG.info("Test rebuildTest passed");
    }

    @Test
    @Transactional
    public void rebuildCropTest() throws Exception {
        LOG.info("Start test-method rebuildCropTest");

        String JSON_OK = "{\"result\":true}";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(JSON_OK);

        mockMvc.perform(post("/api/v0/crops/{shortName}/rebuild", crop.getShortName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectMapper.writeValueAsString(jsonNode)));

        LOG.info("Test rebuildCropTest passed");
    }
}
