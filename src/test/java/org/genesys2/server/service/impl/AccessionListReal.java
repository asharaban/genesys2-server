/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.impl.AccessionList;
import org.genesys2.server.persistence.domain.TraitValueRepository;
import org.genesys2.server.persistence.domain.TraitValueRepositoryImpl;
import org.genesys2.server.service.AccessionListService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.test.GenesysBeansConfig;
import org.genesys2.server.test.JpaRealDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { HazelcastConfig.class, JpaRealDataConfig.class, AccessionListReal.Config.class, GenesysBeansConfig.class }, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
public class AccessionListReal {
	private static final Logger LOG = Logger.getLogger(AccessionListReal.class);
	
	public static class Config {

		@Bean
		public HazelcastCacheManager cacheManager(HazelcastInstance hazelcastInstance) {
			HazelcastCacheManager cm = new HazelcastCacheManager(hazelcastInstance);
			return cm;
		}

		@Bean
		public AccessionListServiceImpl accessionListService() {
			return new AccessionListServiceImpl();
		}

		@Bean
		public TraitValueRepository traitValueRepository() {
			return new TraitValueRepositoryImpl();
		}
	}

	@Autowired
	private AccessionListService accessionListService;

	@Autowired
	private GenesysService genesysService;
	
	@Autowired
	private GenesysFilterService genesysFilterService;

	@Test
	public void testAccessionList1() throws NonUniqueAccessionException {
		LOG.info("Add 1");
		AccessionList accessionList = new AccessionList();
		accessionList.setTitle("List TMb-1");
		accessionListService.save(accessionList);
		assertThat("AccessionList is missing UUID", accessionList.getUuid(), notNullValue());

		AccessionList loaded = accessionListService.getList(accessionList.getUuid());
		assertThat("AccessionList could not be loaded by UUID", loaded, notNullValue());

		// Add TMb-1 to the list
		Accession a1 = genesysService.getAccession("NGA039", "TMb-1");
		assertThat("TMb-1 should not be null", a1, notNullValue());
		accessionListService.addToList(loaded, a1);

		assertThat("List size of 1", accessionListService.sizeOf(loaded), is(1));
		
		// do a load
		Set<Long> ids = accessionListService.getAccessionIds(loaded);
		assertThat("List size of 1", ids.size(), is(1));

		// Bye-bye
		accessionListService.delete(loaded);
	}

	@Test
	public void testAccessionList1to100() throws NonUniqueAccessionException {
		LOG.info("Add 1-100");
		AccessionList accessionList = new AccessionList();
		accessionList.setTitle("List 1-100");
		accessionListService.save(accessionList);
		assertThat("AccessionList is missing UUID", accessionList.getUuid(), notNullValue());

		AccessionList loaded = accessionListService.getList(accessionList.getUuid());
		assertThat("AccessionList could not be loaded by UUID", loaded, notNullValue());

		Set<Long> accessionIds=new HashSet<Long>();
		for (Long acceId : genesysService.listAccessionsIds(new PageRequest(0, 100))) { 
			accessionIds.add(acceId);
		}

		// Add accessions by their IDs
		accessionListService.addToList(loaded, accessionIds);

		Set<Long> ids = accessionListService.getAccessionIds(loaded);
		assertThat("List size of 100", ids.size(), is(100));

		// Bye-bye
		accessionListService.delete(loaded);
	}

	@Test
	public void testAccessionListFromFilters() throws NonUniqueAccessionException {
		LOG.info("Adding by filter");
		AccessionList accessionList = new AccessionList();
		accessionList.setTitle("List NGA039");
		accessionListService.save(accessionList);
		assertThat("AccessionList is missing UUID", accessionList.getUuid(), notNullValue());

		final AccessionList loaded = accessionListService.getList(accessionList.getUuid());
		assertThat("AccessionList could not be loaded by UUID", loaded, notNullValue());

		final AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.INSTCODE).addFilterValue(new FilterHandler.LiteralValueFilter("NGA039")));

		// 1st pass
		accessionListService.addToList(loaded, filters);
		int count = accessionListService.sizeOf(loaded);
		System.err.println(count);

		// 2nd pass (this would throw errors)
		accessionListService.addToList(loaded, filters);
		assertThat("No change in list size expected", accessionListService.sizeOf(loaded), is(count));
	
		final AppliedFilters filters2 = new AppliedFilters();
		filters2.add(new AppliedFilter().setFilterName(FilterConstants.LISTS).addFilterValue(new FilterHandler.LiteralValueFilter(loaded.getUuid())));

		Page<Accession> res = genesysFilterService.listAccessions(filters2, new PageRequest(0, 50));
		assertThat("Page.total must match", res.getTotalElements(), is((long)count));
		assertThat("Page.total must match", res.getNumberOfElements(), is(50));
		
		// Remove all traces of us being here
		accessionListService.delete(loaded);
	}
}
