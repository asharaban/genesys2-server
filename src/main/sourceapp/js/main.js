'use strict';

/*
 * JavaScript for Controllers
 * 
 * @depends on: jQuery 1.7+ @depends on: jQuery Validation plugin 1.9+ @depends
 * on: Handlebars 1.0+ @depends on: Globalize
 * 
 */
  
(function($) {

  // add new functionality to jQuery
  // borrowed from
  // http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
  $.fn.serializeObject = function() {

    var self = this, json = {}, pushCounters = {}, patterns = {
      'validate' : /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
      'key' : /[a-zA-Z0-9_]+|(?=\[\])/g,
      'push' : /^$/,
      'fixed' : /^\d+$/,
      'named' : /^[a-zA-Z0-9_]+$/
    };

    this.build = function(base, key, value) {
      base[key] = value;
      return base;
    };

    this.pushCounter = function(key) {
      if (pushCounters[key] === undefined) {
        pushCounters[key] = 0;
      }
      return pushCounters[key]++;
    };

    $.each($(this).serializeArray(), function() {

      // skip invalid keys
      if (!patterns.validate.test(this.name)) {
        return;
      }

      var k, keys = this.name.match(patterns.key), merge = this.value, reverseKey = this.name;

      while ((k = keys.pop()) !== undefined) {

        // adjust reverseKey
        reverseKey = reverseKey.replace(new RegExp('\\[" + k + "\\]$'), '');

        // push
        if (k.match(patterns.push)) {
          merge = self.build([], self.pushCounter(reverseKey), merge);
        }

        // fixed
        else if (k.match(patterns.fixed)) {
          merge = self.build([], k, merge);
        }

        // named
        else if (k.match(patterns.named)) {
          merge = self.build({}, k, merge);
        }
      }

      json = $.extend(true, json, merge);
    });

    return json;
  };

})(jQuery);
