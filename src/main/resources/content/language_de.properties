#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=Nicht befugt
http-error.401.text=Authentifizierung erforderlich
http-error.403=Zugriff verweigert
http-error.403.text=Sie haben keine Berechtigung, auf diese Ressource zuzugreifen.
http-error.404=Nicht gefunden
http-error.404.text=Die angefragte Ressource konnte nicht gefunden werden, kann aber später wieder verfügbar sein.
http-error.500=Interner Serverfehler
http-error.500.text=Ein unerwarteter Zustand ist aufgetreten, aber keine detailliertere Nachricht ist geeignet.
http-error.503=Dienst nicht verfügbar
http-error.503.text=Der Server ist derzeit nicht verfügbar (weil er überlastet oder aufgrund von Wartungsarbeiten abgeschaltet ist).


# Login
login.username=Benutzername
login.password=Passwort
login.invalid-credentials=Ungültige Zugangsdaten
login.remember-me=Zugangsdaten merken
login.login-button=Anmeldung
login.register-now=Konto erstellen
logout=Abmelden
login.forgot-password=Passwort vergessen
login.with-google-plus=Einloggen mit Google+

# Registration
registration.page.title=Nutzerkonto erstellen
registration.title=Ihr Konto erstellen
registration.invalid-credentials=Ungültige Zugangsdaten.
registration.user-exists=Dieser Benutzername ist bereits vergeben.
registration.email=E-Mail
registration.password=Passwort
registration.confirm-password=Passwort wiederholen
registration.full-name=Ihr vollständiger Name
registration.create-account=Konto erstellen
captcha.text=Captcha-Text


id=ID

name=Name
description=Beschreibung
actions=Aktionen
add=Hinzufügen
edit=Bearbeiten
save=Speichern
create=Erstellen
cancel=Abbruch
delete=Löschen

jump-to-top=Zurück zum Anfang\!

pagination.next-page=Vor >
pagination.previous-page=< Zurück


# Language
locale.language.change=Region ändern
i18n.content-not-translated=Dieser Inhalt ist in Ihrer Sprache nicht verfügbar. Kontaktieren Sie uns bitte, wenn Sie bei der Übersetzung helfen können\!

data.error.404=Die von Ihnen angefragten Daten wurden im System nicht gefunden.
page.rendertime=Das Bearbeiten dieser Seite dauerte {0} Min.

footer.copyright-statement=&copy; 2013 - 2015 Datenanbieter und der Crop Trust

menu.home=Home
menu.browse=Browsen
menu.datasets=C&E-Daten
menu.descriptors=Deskriptoren
menu.countries=Länder
menu.institutes=Institute
menu.my-list=Meine Liste
menu.about=Über Genesys
menu.contact=Kontakt
menu.disclaimer=Haftungsausschluss
menu.feedback=Feedback
menu.help=Hilfe
menu.terms=Nutzungsbedingungen
menu.copying=Copyright
menu.privacy=Datenschutzrichtlinie
menu.newsletter=Genesys-Newsletter
menu.join-the-community=Der Genesys-Community beitreten
menu.faq=Häufige Fragen
menu.news=Nachrichten
page.news.title=Nachrichten
page.news.intro=Das Neueste aus der Genesys-Community – von Einzelheiten zu unseren neuesten Mitgliedern bis zu Updates über Akzessionen, die weltweit erhalten werden.

# Extra content
menu.what-is-genesys=Wer oder was ist Genesys?
menu.who-uses-genesys=Wer nutzt Genesys?
menu.how-to-use-genesys=Wie wird Genesys genutzt?
menu.history-of-genesys=Die Geschichte von Genesys
menu.about-genesys-data=Über Genesys-Daten



page.home.title=Genesys PGR

user.pulldown.administration=Verwaltung
user.pulldown.users=Benutzerliste
user.pulldown.logout=Abmelden
user.pulldown.profile=Mein Profil
user.pulldown.oauth-clients=OAuth-Clients
user.pulldown.teams=Teams
user.pulldown.manage-content=Inhalt verwalten

user.pulldown.heading={0}
user.create-new-account=Ein Konto erstellen
user.full-name=Vollständiger Name
user.email=E-Mail-Adresse
user.account-status=Kontostatus
user.account-disabled=Konto deaktiviert
user.account-locked-until=Konto gesperrt bis
user.roles=Benutzeraufgaben
userprofile.page.title=Benutzerprofil
userprofile.page.intro=Als Gemeinschaft hängt der Erfolg von Genesys von seinen Nutzern ab. Ob als einzelner Forscher oder als Vertreter eines großen Instituts\: Hier können Sie Ihr Profil aktualisieren, Ihre Interessen zeigen und Ihre Sammlungsdaten präsentieren.
userprofile.update.title=Ihr Profil aktualisieren

user.page.list.title=Registrierte Benutzerkonten

crop.croplist=Anbaupflanzen-Liste
crop.all-crops=Alle Anbaupflanzen
crop.taxonomy-rules=Taxonomie-Regeln
crop.view-descriptors=Anbaupflanzen-Deskriptoren ansehen ...
crop.page.edit.title=Bearbeiten {0}
crop.summary=Zusammenfassung (HTML-Metadaten)

activity.recent-activity=Kürzliche Aktivität

country.page.profile.title=Länderprofil\: {0}
country.page.list.title=Länderliste
country.page.list.intro=Durch Anklicken eines Ländernamens können Sie die in dem Land registrierten Institute sowie Statistiken zu pflanzengenetischen Ressourcen für das Land als Ganzes sehen.
country.page.not-current=Dies ist ein historischer Eintrag.
country.page.faoInstitutes={0} Institute bei WIEWS registriert
country.stat.countByLocation={0} Akzessionen von Instituten dieses Landes
country.stat.countByOrigin={0} in Genesys registrierte Akzessionen dieses Landes.
country.statistics=Länderstatistik
country.accessions.from=In {0} gesammelte Akzessionen
country.more-information=Weitere Informationen\:
country.replaced-by=Ländercode wird ersetzt durch\: {0}
country.is-itpgrfa-contractingParty={0} ist Vertragspartei des Internationalen Vertrags über pflanzengenetische Ressourcen für Ernährung und Landwirtschaft (ITPGRFA).
select-country=Land wählen

project.page.list.title=Projekte
project.page.profile.title={0}
project.code=Projektcode
project.name=Projektname
project.url=Projekt-Website
project.summary=Zusammenfassung (HTML-Metadaten)
project.accessionLists=Akzessionslisten


faoInstitutes.page.list.title=WIEWS-Institute
faoInstitutes.page.profile.title=WIEWS {0}
faoInstitutes.stat.accessionCount=Akzession in Genesys\:
faoInstitutes.stat.datasetCount=Weitere Datensätze in Genesys\:
faoInstitute.stat-by-crop=Am meisten vorkommende Anbaupflanzen
faoInstitute.stat-by-genus=Häufigste Gattungen
faoInstitute.stat-by-species=Häufigste Arten
faoInstitute.accessionCount={0} Akzessionen
faoInstitute.statistics=Institutsstatistik
faoInstitutes.page.data.title=Akzessionen in {0}
faoInstitute.accessions.at=Akzessionen bei {0}
faoInstitutes.viewAll=Alle registrierten Institute anzeigen
faoInstitutes.viewActiveOnly=Institute mit Akzessionen in Genesys anzeigen
faoInstitute.no-accessions-registered=Bitte kontaktieren Sie uns, wenn Sie dabei behilflich sein können, Daten von diesem Institut bereitzustellen.
faoInstitute.institute-not-current=Diese Aufzeichnung ist archiviert.
faoInstitute.view-current-institute=Zu {0} navigieren.
faoInstitute.country=Land
faoInstitute.code=WIEWS-Code
faoInstitute.email=Kontakt-E-Mail
faoInstitute.acronym=Abkürzung
faoInstitute.url=Weblink
faoInstitute.summary=Zusammenfassung (HTML-Metadaten)
faoInstitute.member-of-organizations-and-networks=Organisationen und Netzwerke\:
faoInstitute.uniqueAcceNumbs.true=Jede Akzessionsnummer ist innerhalb dieses Instituts nur einmal vergeben
faoInstitute.uniqueAcceNumbs.false=Die gleiche Akzessionsnummer kann in unterschiedlichen Sammlungen dieses Instituts verwendet werden.
faoInstitute.requests.mailto=E-Mail-Adresse für Materialanfragen\:
faoInstitute.allow.requests=Materialanfragen gestatten
faoInstitute.sgsv=SGSV-Code
faoInstitute.data-title=Informationsdatenbank über Akzessionen wird gepflegt bei {0}
faoInstitute.overview-title=Überblick über Informationen zu  Akzessionen wird gepflegt bei {0}
faoInstitute.datasets-title=Charakterisierungs- und Bewertungsdaten bereitgestellt durch {0}
faoInstitute.meta-description=Überblick über pflanzengenetische Ressourcen wird gepflegt in Sammlungen bei {0} ({1}) Genbank
faoInstitute.link-species-data=Siehe Informationen zu {2} Akzessionen bei {0} ({1})
faoInstitute.wiewsLink={0} Einzelheiten auf FAO WIEWS-Website

view.accessions=Akzessionen browsen
view.datasets=Datensätze ansehen
paged.pageOfPages=Seite {0} von {1}
paged.totalElements={0} Einträge

accessions.number={0} Akzessionen
accession.metadatas=C&E-Daten
accession.methods=Charakterisierungs- und Evaluierungsdaten
unit-of-measure=Maßeinheit

ce.trait=Merkmal
ce.sameAs=Gleich wie
ce.methods=Methoden
ce.method=Methode
method.fieldName=Datenbankfeld

accession.uuid=UUID
accession.accessionName=Akzessionsnummer
accession.origin=Herkunftsland
accession.holdingInstitute=Bereitstellendes Institut
accession.holdingCountry=Ort
accession.taxonomy=Wissenschaftlicher Name
accession.crop=Name Anbaupflanze
accession.otherNames=Auch bekannt als
accession.inTrust=Treuhänderisch
accession.mlsStatus=MLS-Status
accession.duplSite=Institut für Sicherheitsduplikation
accession.inSvalbard=Sicherheitsdupliziert in der Svalbard-Saatgutbank
accession.inTrust.true=Diese Akzession fällt unter Artikel 15 des internationalen Vertrags über pflanzengenetische Ressourcen für Ernährung und Landwirtschaft.
accession.mlsStatus.true=Diese Akzession ist im multilateralen System des Internationalen Vertrags für pflanzengenetische Ressourcen für Ernährung und Landwirtschaft (ITPGRFA)
accession.inSvalbard.true=Sicherheitsduplikat in Svalbard-Datenbank - dem weltweiten Saatgut-Tresor auf Spitzbergen
accession.not-available-for-distribution=Die Akzession ist NICHT abgebbar.
accession.this-is-a-historic-entry=Dies ist eine historische Aufzeichnung einer Akzession.
accession.historic=Historischer Eintrag
accession.available-for-distribution=Die Akzession ist abgebbar.
accession.elevation=Höhe über Meeresspiegel
accession.geolocation=Geographische Koordinaten (Breitengrad, Längengrad)

accession.storage=Art der Protoplasma-Speicherung
accession.storage.=
accession.storage.10=Samensammlung
accession.storage.11=Kurzfristige Samensammlung
accession.storage.12=Mittelfristige Samensammlung
accession.storage.13=Langfristige Samensammlung
accession.storage.20=Sammlung Feld
accession.storage.30=In-vitro-Sammlung
accession.storage.40=Sammlung in Kryokonservierung
accession.storage.50=DNA-Sammlung
accession.storage.99=Sonstiges

accession.breeding=Züchterangaben
accession.breederCode=Züchter-Code
accession.pedigree=Stammbaum
accession.collecting=Sammelinformationen
accession.collecting.site=Ort der Sammlungsseite
accession.collecting.institute=Sammelndes Institut
accession.collecting.number=Sammlungs-Nummer
accession.collecting.date=Datum der Probensammlung
accession.collecting.mission=ID der Sammelreise
accession.collecting.source=Herkunft der Erfassung/des Erwerbs

accession.collectingSource.=
accession.collectingSource.10=Natürlicher Fundort
accession.collectingSource.11=Wald oder Waldgebiet
accession.collectingSource.12=Busch/Savanne
accession.collectingSource.13=Prärie/Steppe/Grasland
accession.collectingSource.14=Wüste/Tundra
accession.collectingSource.15=Feuchtgebiet/Sumpf/Gewässer
accession.collectingSource.20=Ackerbaulich genutzter Fundort
accession.collectingSource.21=Acker
accession.collectingSource.22=Garten
accession.collectingSource.23=Hof, Küchen- oder Hausgarten (urban, suburban oder ländlich)
accession.collectingSource.24=Brachland
accession.collectingSource.25=Weide
accession.collectingSource.26=Hofladen/Direktvermarkter
accession.collectingSource.27=Dreschplatz
accession.collectingSource.28=Park
accession.collectingSource.30=Markt oder Ladengeschäft
accession.collectingSource.40=(Forschungs-)Institut/Station/Organisation, Genbank
accession.collectingSource.50=Saatzuchtbetrieb/Züchter
accession.collectingSource.60=Begleitflora, Ödland oder Ruderalstelle
accession.collectingSource.61=Straßen-/Wegrand
accession.collectingSource.62=Feldrand
accession.collectingSource.99=Sonstiger Fundort

accession.donor.institute=Spenderinstitut
accession.donor.accessionNumber=Akzessions-ID des Spenders
accession.geo=Geographische Angaben
accession.geo.datum=Koordinatendatum
accession.geo.method=Georeferenzierungsmethode
accession.geo.uncertainty=Geographische Unschärfe
accession.sampleStatus=Biologischer Status der Akzession

accession.sampleStatus.=
accession.sampleStatus.100=Wild
accession.sampleStatus.110=Natürlich
accession.sampleStatus.120=Halb-natürlich/-wild
accession.sampleStatus.130=Halb-natürlich/gesät
accession.sampleStatus.200=Begleitflora, Unkraut
accession.sampleStatus.300=Herkömmliche Sorte/Landrasse
accession.sampleStatus.400=Zucht-/Forschungsmaterial
accession.sampleStatus.410=Zuchtlinie
accession.sampleStatus.411=künstliche Population
accession.sampleStatus.412=Hybrid
accession.sampleStatus.413=Gründerbestand/Gründerpopulation
accession.sampleStatus.414=Inzuchtlinie
accession.sampleStatus.415=Abgetrennte Population
accession.sampleStatus.416=Klonale Selektion
accession.sampleStatus.420=Genbestand
accession.sampleStatus.421=Mutant
accession.sampleStatus.422=Zytogenetische Bestände
accession.sampleStatus.423=Sonstige genetischen Bestände
accession.sampleStatus.500=Erweiterte/verbesserte Kultur
accession.sampleStatus.600=GVO
accession.sampleStatus.999=Sonstiges

accession.availability=Abgebbar
accession.aliasType.ACCENAME=Akzessionsname
accession.aliasType.DONORNUMB=Akzessions-ID des Spenders
accession.aliasType.BREDNUMB=Vom Züchter vergebener Name
accession.aliasType.COLLNUMB=Sammlungs-Nummer
accession.aliasType.OTHERNUMB=Andere Namen
accession.aliasType.DATABASEID=(Datenbank-ID)
accession.aliasType.LOCALNAME=(Lokaler Name)

accession.availability.=Unbekannt
accession.availability.true=Verfügbar
accession.availability.false=Nicht verfügbar

accession.historic.true=Historisch
accession.historic.false=Aktiv
accession.acceUrl=Zusätzliche Akzessions-URL

accession.page.profile.title=Profil der Akzession\: {0}
accession.page.resolve.title=Mehrere Akzessionen gefunden
accession.resolve=Mehrere Akzessionen mit dem Namen "{0}" in Genesys gefunden. Wählen Sie einen aus der Liste aus.
accession.page.data.title=Akzessions-Browser
accession.page.data.intro=Erforschen Sie die auf Genesys vorliegenden Akzessionsdaten. Zur detaillierten Suche können Sie Filter hinzufügen (z. B. Ursprungsland, Nutzpflanze, Gattung oder Breiten- und Längengrad der Sammelstelle).
accession.taxonomy-at-institute=Ansicht {0} von {1}

accession.svalbard-data=Duplikationsdaten der Svalbard-Datenbank, des weltweiten Saatgut-Tresors auf Spitzbergen
accession.svalbard-data.taxonomy=Gemeldeter wissenschaftlicher Name
accession.svalbard-data.depositDate=Ablagedatum
accession.svalbard-data.boxNumber=Behälternummer
accession.svalbard-data.quantity=Menge
accession.remarks=Bemerkungen

taxonomy.genus=Gattung
taxonomy.species=Art
taxonomy.taxonName=Wissenschaftlicher Name
taxonomy-list=Taxonomie-Liste

selection.page.title=Ausgewählte Akzessionen
selection.page.intro=Erstellen Sie beim Durchsuchen der Millionen von auf Genesys gespeicherten Akzessionen Ihre eigene Liste, und halten Sie Ihre Suchergebnisse fest. Ihre Auswahlen werden gespeichert, und Sie können jederzeit dort weitermachen, wo Sie aufgehört haben.
selection.add={0} zur Liste hinzufügen
selection.remove={0} aus Liste entfernen
selection.clear=Listeneinträge löschen
selection.reload-list=Liste neu laden
selection.map=Akzessionskarte anzeigen
selection.send-request=Protoplasma-Anfrage senden
selection.empty-list-warning=Sie haben der Liste keine Akzessionen hinzugefügt.
selection.add-many=Mehrere Akzessionen hinzufügen
selection.add-many.accessionIds=Akzessionsnummern (ACCENUMB) so aufführen, wie in Genesys verzeichnet (mit Komma oder Semikolon getrennt oder in neuer Zeile).
selection.add-many.instCode=WIEWS-Code bereitstellendes Institut
selection.add-many.button=Zur Auswahlliste hinzufügen

savedmaps=Aktuelle Karte merken
savedmaps.list=Kartenliste
savedmaps.save=Karte merken

filter.enter.title=Filtertitel eingeben
filters.page.title=Daten-Filter
filters.view=Aktuelle Filter
filter.filters-applied=Sie haben Filter ausgewählt.
filter.filters-not-applied=Sie können die Daten filtern
filters.data-is-filtered=Die Daten sind gefiltert.
filters.toggle-filters=Filter
filter.taxonomy=Wissenschaftlicher Name
filter.art15=ITPGRFA Art. 15 Akzession
filter.acceNumb=Akzessionsnummer
filter.seqNo=Erkannte laufende Nummer
filter.alias=Akzessionsname
filter.crops=Anbaupflanzenname
filter.orgCty.iso3=Herkunftsland
filter.institute.code=Name bereitstellendes Institut
filter.institute.country.iso3=Land führendes Institut
filter.sampStat=Biologischer Status der Akzession
filter.institute.code=Name bereitstellendes Institut
filter.geo.latitude=Breitengrad
filter.geo.longitude=Längengrad
filter.geo.elevation=Höhe
filter.taxonomy.genus=Genus
filter.taxonomy.species=Spezies
filter.taxonomy.subtaxa=Teilgruppen (Subtaxa)
filter.taxSpecies=Spezies
filter.taxonomy.sciName=Wissenschaftlicher Name
filter.sgsv=Sicherheitsdupliziert in der Svalbard-Saatgutbank
filter.mlsStatus=MLS-Status der Akzession
filter.available=Abgebbar
filter.historic=Historische Aufzeichnung
filter.donorCode=Spendeninstitut
filter.duplSite=Sicherheitsduplikatsseite
filter.download-dwca=ZIP herunterladen
filter.download-mcpd=MCPD herunterladen
filter.add=Filter hinzufügen
filter.additional=Weitere Filter
filter.apply=Anwenden
filter.close=Schließen
filter.remove=Filter entfernen
filter.autocomplete-placeholder=Mehr als 3 Zeichen eingeben ...
filter.coll.collMissId=ID der Sammelreise
filter.storage=Art der Protoplasmaspeicherung
filter.string.equals=Ist gleich
filter.string.like=Beginnt mit
filter.inverse=Außer
filter.set-inverse=Ausgewählte Werte ausschließen
filter.internal.message.like=Gefällt mir {0}
filter.internal.message.between=Zwischen {0}
filter.internal.message.and={0} und {0}
filter.internal.message.more=Mehr als {0}
filter.internal.message.less=Weniger als {0}
filter.lists=In Akzessionsliste aufgeführt


search.page.title=Volltextsuche
search.no-results=Für Ihre Anfrage wurden keine Treffer gefunden.
search.input.placeholder=Genesys durchsuchen ...
search.search-query-missing=Geben Sie bitte Ihre Suchanfrage ein.
search.search-query-failed=Es tut uns leid, leider ist die Suche aufgrund von Fehler {0} fehlgeschlagen
search.button.label=Suche
search.add-genesys-opensearch=Genesys-Suche in Ihrem Browser registrieren

admin.page.title=Genesys-2-Administration
metadata.page.title=Datensätze
metadata.page.intro=Blättern Sie durch Merkmals- und Beurteilungsdatensätze, die von Forschern und Forschungsorganisationen auf Genesys hochgeladen wurden. Zu jedem Datensatz können Sie die Daten im Excel- oder im CVS-Format herunterladen.
metadata.page.view.title=Datensatzangaben
metadata.download-dwca=ZIP herunterladen
page.login=Anmeldung

traits.page.title=Deskriptoren
trait-list=Deskriptoren


organization.page.list.title=Organisationen und Netzwerke
organization.page.profile.title={0}
organization.slug=Abkürzung von Organisation oder Netzwerk
organization.title=Vollständiger Name
organization.summary=Zusammenfassung (HTML-Metadaten)
filter.institute.networks=Netzwerk


menu.report-an-issue=Ein Problem melden
menu.scm=Quellcode
menu.translate=Genesys übersetzen

article.edit-article=Artikel wird bearbeitet
article.slug=Artikel-Slug (URL)
article.title=Artikelüberschrift
article.body=Artikeltext
article.summary=Zusammenfassung (HTML-Metadaten)
article.post-to-transifex=Auf Transifex stellen
article.remove-from-transifex=Aus Transifex entfernen
article.fetch-from-transifex=Übersetzungen abrufen
article.transifex-resource-updated=Die Ressource wurde auf Transifex erfolgreich aktualisiert.
article.transifex-resource-removed=Die Ressource wurde von Transifex erfolgreich entfernt.
article.transifex-failed=Beim Datenaustausch mit Transifex ist ein Fehler aufgetreten
article.translations-updated=Ressource wurde mit übersetzten Daten erfolgreich aktualisiert\!
article.share=Diesen Artikel teilen

user.accession.list.saved-updated=Ihre Akzessionsliste wurde erfolgreich gespeichert.
user.accession.list.deleted=Ihre Akzessionsliste wurde erfolgreich vom Server gelöscht.
user.accession.list.create-update=Speichern
user.accession.list.delete=Löschen
user.accession.list.title=Akzessionsliste

activitypost=Aktivitätsbeitrag
activitypost.add-new-post=Neuen Beitrag hinzufügen
activitypost.post-title=Beitragsüberschrift
activitypost.post-body=Text

blurp.admin-no-blurp-here=Keine weiteren Erklärungen verfügbar.
blurp.blurp-title=Überschrift weitere Erklärungen
blurp.blurp-body=Inhalt weitere Erklärungen
blurp.update-blurp=Weitere Erklärungen speichern


oauth2.confirm-request=Zugriff bestätigen
oauth2.confirm-client=Sie, <b>{0}</b>, ermächtigen hiermit <b>{1}</b>, auf Ihre geschützten Ressourcen zuzugreifen.
oauth2.button-approve=Ja, Zugriff gestatten
oauth2.button-deny=Nein, Zugriff verweigern

oauth2.authorization-code=Autorisierungscode
oauth2.authorization-code-instructions=Kopieren Sie diesen Autorisierungscode\:

oauth2.access-denied=Zugriff verweigert
oauth2.access-denied-text=Sie haben den Zugriff auf Ihre Ressourcen verweigert.

oauth-client.page.list.title=OAuth2-Clients
oauth-client.page.profile.title=OAuth2-Client\: {0}
oauth-client.active-tokens=Liste mit ausgegebenen Token
client.details.title=Client-Titel
client.details.description=Beschreibung

maps.loading-map=Karte wird geladen...
maps.view-map=Karte anzeigen
maps.accession-map=Akzessionskarte
maps.accession-map.intro=Die Karte zeigt die Georeferenzen der Akzessions-Sammelstellen.

audit.createdBy=Erstellt von  {0}
audit.lastModifiedBy=Zuletzt aktualisiert von {0}

itpgrfa.page.list.title=Vertragsparteien des Internationalen Vertrags für pflanzengenetische Ressourcen für Ernährung und Landwirtschaft (ITPGRFA)

request.page.title=Material von bereitstellenden Instituten anfragen
request.total-vs-available=Von {0} gelisteten Akzessionen ist von {1} bekannt, dass sie abgebbar ist/sind.
request.start-request=Verfügbares Protoplasma anfragen
request.your-email=Ihre E-Mail-Adresse\:
request.accept-smta=SMTA-/MTA-Akzeptanz\:
request.smta-will-accept=Ich akzeptiere die Allgemeinen Geschäftsbedingungen der SMTA/MTA
request.smta-will-not-accept=Ich akzeptiere NICHT die Allgemeinen Geschäftsbedingungen der SMTA/MTA
request.smta-not-accepted=Sie haben nicht angegeben, ob Sie die Allgemeinen Geschäftsbedingungen der SMTA/MTA akzeptieren. Ihre Materialanfrage wird nicht bearbeitet.
request.purpose=Materialverwendung spezifizieren\:
request.purpose.0=Sonstiges (bitte im Hinweisfeld erklären)
request.purpose.1=Lebensmittel- und Agrikulturforschung
request.notes=Weitere Informationen und Hinweise eingeben\:
request.validate-request=Ihre Anfrage verifizieren
request.confirm-request=Erhalt der Anfrage bestätigen
request.validation-key=Verifizierungsschlüssel\:
request.button-validate=Verifizieren
validate.no-such-key=Verifizierungsschlüssel ist nicht gültig.

team.user-teams=Benutzerteams
team.create-new-team=Ein neues Team erstellen
team.team-name=Teamname
team.leave-team=Team verlassen
team.team-members=Teammitglieder
team.page.profile.title=Team\: {0}
team.page.list.title=Alle Teams
validate.email.key=Schlüssel eingeben
validate.email=E-Mail-Verifizierung
validate.email.invalid.key=Schlüssel ungültig

edit-acl=Berechtigungen bearbeiten
acl.page.permission-manager=Berechtigungsleiter
acl.sid=Sicherheitsidentität
acl.owner=Objektinhaber
acl.permission.1=Lesen
acl.permission.2=Schreiben
acl.permission.4=Erstellen
acl.permission.8=Löschen
acl.permission.16=Organisieren


ga.tracker-code=GA Suchcode

boolean.true=Ja
boolean.false=Nein
boolean.null=Unbekannt
userprofile.password=Passwort zurücksetzen
userprofile.enter.email=Ihre E-Mail eingeben
userprofile.enter.password=Neues Passwort eingeben
userprofile.email.send=E-Mail senden

verification.invalid-key=Token-Schlüssel ist nicht gültig
verification.token-key=Verifizierungsschlüssel
login.invalid-token=Ungültiger Zugangs-Token

descriptor.category=Deskriptorkategorie
method.coding-table=Kodiertabelle

oauth-client.info=Client-Info
oauth-client.list=Liste mit OAuth-Clients
client.details.client.id=Einzelheiten Client-ID
client.details.additional.info=Zusatzinfo
client.details.token.list=Token-Liste
client.details.refresh-token.list=Liste der aktualisieren Token
oauth-client.remove=Entfernen
oauth-client.remove.all=Alle entfernen
oauth-client=Client
oauth-client.token.issue.date=Ausgabedatum
oauth-client.expires.date=Ablaufdatum
oauth-client.issued.tokens=Ausgestellte Token
client.details.add=OAuth-Client hinzufügen
oauth-client.create=OAuth-Client erstellen
oauth-client.id=Client-ID
oauth-client.secret=Client secret
oauth-client.redirect.uri=Client Redirect URI
oauth-client.access-token.accessTokenValiditySeconds=Gültigkeit Zugangstoken
oauth-client.access-token.refreshTokenValiditySeconds=Gültigkeit des aktualisierten Tokens
oauth-client.access-token.defaultDuration=Default verwenden
oauth-client.title=Client-Titel
oauth-client.description=Beschreibung
oauth2.error.invalid_client=Ungültige Client-ID. Verifizieren Sie Ihre Client_ID und Client_Secret Parameter.

team.user.enter.email=Benutzer-E-Mail eingeben
user.not.found=Benutzer nicht gefunden
team.profile.update.title=Teaminformationen aktualisieren

autocomplete.genus=Gattung finden ...

stats.number-of-countries={0} Länder
stats.number-of-institutes={0} Institute
stats.number-of-accessions={0} Akzessionen

navigate.back=Zurück


content.page.list.title=Artikelliste
article.lang=Sprache
article.classPk=Objekt

session.expiry-warning-title=Warnung Sitzungsende
session.expiry-warning=Ihre aktuelle Sitzung läuft in Kürze ab. Möchten Sie diese Sitzung verlängern?
session.expiry-extend=Sitzung verlängern

download=Download
download.kml=KML herunterladen

data-overview=Statistischer Überblick
data-overview.intro=Ein statistischer Überblick über die auf Genesys gespeicherten Informationen für die zurzeit eingestellten Filter - von der Gesamtzahl der Akzessionen je Land bis zur Artenvielfalt von Sammlungen.
data-overview.short=Überblick
data-overview.institutes=Bereitstellende Institute
data-overview.composition=Zusammensetzung der Genbank-Bereitsteller
data-overview.sources=Materialquellen
data-overview.management=Sammelmanagement
data-overview.availability=Materialverfügbarkeit
data-overview.otherCount=Sonstiges
data-overview.missingCount=Nicht spezifiziert
data-overview.totalCount=Gesamt
data-overview.donorCode=FAO WIEWS Code des Spendeninstituts
data-overview.mlsStatus=Unter MLS abgebbar


admin.cache.page.title=Überblick Anwendungs-Cache
cache.stat.map.ownedEntryCount=Eingaben eigener Cache
cache.stat.map.lockedEntryCount=Eingaben gesperrter Cache
cache.stat.map.puts=Anzahl Einträge im Cache
cache.stat.map.hits=Anzahl Cache-Treffer

loggers.list.page=Liste konfigurierter Datenlogger
logger.add-logger=Datenlogger hinzufügen
logger.edit.page=Konfigurationsseite Datenlogger
logger.name=Name Datenlogger
logger.log-level=Level loggen
logger.appenders=Log-Appender

menu.admin.index=Admin
menu.admin.loggers=Datenlogger
menu.admin.caches=Cache
menu.admin.ds2=DS2-Datensätze

news.content.page.all.title=Nachrichtenliste
news.archive.title=Nachrichtenarchiv

worldclim.monthly.title=Klima an Sammelstelle
worldclim.monthly.precipitation.title=Monatlicher Niederschlag
worldclim.monthly.temperatures.title=Monatliche Temperaturen
worldclim.monthly.precipitation=Monatlicher Niederschlag [mm]
worldclim.monthly.tempMin=Mindesttemperatur [°C]
worldclim.monthly.tempMean=Temperaturdurchschnitt [°C]
worldclim.monthly.tempMax=Höchsttemperatur [°C]

worldclim.other-climate=Sonstige Klimadaten

download.page.title=Vor dem Download
download.download-now=Beginnen Sie den Download, ich warte.

resolver.page.index.title=Permanente ID finden
resolver.acceNumb=Akzessionsnummer
resolver.instCode=Code bereitstellendes Institut
resolver.lookup=ID finden
resolver.uuid=UUID
resolver.resolve=Klären

resolver.page.reverse.title=Ergebnisse des Lösungsansatzes
accession.purl=Permanente URL

menu.admin.kpi=KPI
admin.kpi.index.page=KPI
admin.kpi.execution.page=KPI-Ausführung
admin.kpi.executionrun.page=Einzelheiten zur Ausführung

accession.pdci=Vollständigkeitsindex der Passdaten
accession.pdci.jumbo=PDCI-Score\: {0,number,0.00} von 10,0
accession.pdci.institute-avg=Der durchschnittliche PDCI-Score für dieses Institut ist {0,number,0.00}
accession.pdci.about-link=Über Vollständigkeitsindex der Passdaten lesen
accession.pdci.independent-items=Unabhängig vom Populationstyp
accession.pdci.dependent-items=Abhängig vom Populationstyp
accession.pdci.stats-text=Der durchschnittliche PDCI-Score für {0} Akzessionen ist {1,number,0.00}, mit einem Mindestscore von {2,number,0.00} und einem Höchstscore von {3,number,0.00}.

accession.donorNumb=Code Spenderinstitut
accession.acqDate=Erwerbsdatum

accession.svalbard-data.url=Datenbank-URL Svalbard
accession.svalbard-data.url-title=Ablageinformationen in SGSV-Datenbank
accession.svalbard-data.url-text=SGSV-Ablageinformationen ansehen zu {0}

filter.download-pdci=PDCI-Daten herunterladen
statistics.phenotypic.stats-text=Von den {0} Akzessionen hat/haben {1} Akzessionen ({2,number,0.##%}) mindestens ein weiteres, in einem Datensatz verzeichnetes Merkmal, das auf Genesys erhältlich ist (durchschnittlich {3,number,0.##} Merkmale in {4,number,0.##} Datensätze).

twitter.tweet-this=Twittern\!
twitter.follow-X=Folgen auf @{0}
linkedin.share-this=Auf LinkedIn teilen
share.link=Link teilen
share.link.placeholder=Bitte warten ...
share.link.text=Verwenden Sie bitte die Kurzversion der vollständigen URL dieser Seite\:

welcome.read-more=Mehr über Genesys lesen
twitter.latest-on-twitter=Das Neueste auf Twitter
video.play-video=Video abspielen

heading.general-info=Allgemeine Informationen
heading.about=Über uns
heading.see-also=Siehe auch
see-also.map=Karte der Akzessions-Standorte
see-also.overview=Überblick über die Sammlung
see-also.country=Mehr über die PGR-Erhaltung in {0}

help.page.intro=Besuchen Sie den Abschnitt „Tutorials“. Dort finden Sie Informationen zur Benutzung von Genesys.

charts=Diagramme und Karten
charts.intro=Hier finden Sie hilfreiche Diagramme und Karten für Ihre nächste Präsentation\!
chart.collections.title=Größe der Genbank-Sammlungen
chart.attribution-text=www.genesys-pgr.org
chart.collections.series=Anzahl der Akzessionen in Genbanken

label.load-more-data=Mehr laden ...

userlist.list-my-lists=Listen der gespeicherten Akzessionen
userlist.title=Listenname
userlist.description=Listenbeschreibung
userlist.disconnect=Liste abkoppeln
userlist.update-list=Aktualisierte Liste speichern
userlist.make-new-list=Neue Liste erstellen
userlist.shared=Zugriff auf Liste erlauben

region.page.list.title = Geographische FAO-Regionen
region.page.list.intro = Die nachstehenden Listen mit geographischen FAO-Regionen ermöglichen Ihnen den Zugriff auf Akzessionsdaten, die in diesen Regionen erfasst oder gepflegt werden.
region.page.show.parent = Elternregion {0} anzeigen
region.page.show.world = Alle Weltregionen anzeigen
region.countries-in-region=Länderliste in {0}
region.regions-in-region=FAO-Regionen in {0}
region.show-all-regions=Alle FAO-Regionen aufführen

