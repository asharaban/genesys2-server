package org.genesys2.server.servlet.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.GeoRegion;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.GeoRegionService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/geo/regions")
public class GeoRegionController extends BaseController {

    @Autowired
    GeoRegionService geoRegionService;

    @Autowired
    CountryRepository countryRepository;

	@Autowired
	private ContentService contentService;

    @RequestMapping
    public String list() {

        return "/region/index";
    }

    @RequestMapping("/{isoCode}")
    public String view(ModelMap model, @PathVariable(value = "isoCode") String code) {

        GeoRegion geoRegion = geoRegionService.find(code);

        if (geoRegion == null) {
            _logger.error("Can't find region for region code " + code);
            throw new ResourceNotFoundException("Can't find region for region code " + code);
        }

        List<Country> countryList = geoRegion.getCountries().stream().filter(country -> isCurrent(country)).collect(Collectors.toList());
        Country.sort(countryList, getLocale());

        model.addAttribute("region", geoRegion);
		model.addAttribute("blurp", contentService.getArticle(geoRegion, "blurp", getLocale()));
        List<GeoRegion> subRegions = geoRegionService.getChildren(geoRegion.getIsoCode());
        GeoRegion.sort(subRegions, getLocale());
        model.addAttribute("subRegions", subRegions);
        
        model.addAttribute("countries", countryList);

        return "/region/region";
    }

    private boolean isCurrent(Country country) {
		return country.isCurrent();
	}
    

	@RequestMapping("/{isoCode}/edit")
	public String edit(ModelMap model, @PathVariable(value = "isoCode") String code) {
		view(model, code);
		return "/region/edit";
	}

	@RequestMapping("/{isoCode}/update")
	public String update(ModelMap model, @PathVariable(value = "isoCode") String code, @RequestParam("blurp") String body,
			@RequestParam(value = "summary", required = false) String summary) {

		_logger.debug("Updating region " + code);
		final GeoRegion region = geoRegionService.find(code);
		if (region == null) {
			throw new ResourceNotFoundException();
		}

		contentService.updateArticle(region, "blurp", null, body, summary, getLocale());

		return "redirect:/geo/regions/" + code;
	}

}
