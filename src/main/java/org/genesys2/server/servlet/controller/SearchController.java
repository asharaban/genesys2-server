/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.impl.SearchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
// @Scope("request")
public class SearchController {
	public static final Log LOG = LogFactory.getLog(SearchController.class);

	@Autowired
	ElasticService searchService;

	@Value("${base.url}")
	private String baseUrl;

	@RequestMapping(value = "/acn/opensearch/desc", method = RequestMethod.GET)
	public String openSearchDesc(ModelMap model) {
		model.addAttribute("baseUrl", baseUrl);
		return "/search/opensearch-desc";
	}

	@RequestMapping(value = "/acn/opensearch", produces = MediaType.APPLICATION_XML_VALUE)
	public String openSearch(ModelMap model, @RequestParam(required = false, value = "q") String searchQuery,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) throws IOException {

		model.addAttribute("baseUrl", baseUrl);
		model.addAttribute("q", searchQuery);

		if (!StringUtils.isBlank(searchQuery)) {
			try {
				final Page<?> x = searchService.search(searchQuery, new PageRequest(page - 1, 50));
				model.addAttribute("pagedData", x);
				LOG.info("Searching for: " + searchQuery + " returns " + x.getNumberOfElements());
			} catch (SearchException e) {
				LOG.info("Searching for: " + searchQuery + " failed with error " + e.getMessage());
				LOG.error(e.getMessage(), e);
				model.addAttribute("error", e);
			}
		}

		return "/search/opensearch";
	}

	/**
	 * OpenSearch autocompleter
	 * 
	 * @param searchQuery
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/acn/opensearch-gossip", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	List<Object> openSearchAutocomplete(ModelMap model, @RequestParam(required = false, value = "q") String searchQuery) throws IOException {
		List<Object> res = new ArrayList<Object>();
		res.add(searchQuery);

		if (!StringUtils.isBlank(searchQuery)) {
			try {
				res.add(searchService.autocompleteSearch(searchQuery));
			} catch (SearchException e) {
				res.add(ArrayUtils.EMPTY_STRING_ARRAY);
			}
		} else {
			res.add(ArrayUtils.EMPTY_STRING_ARRAY);
		}

		res.add(ArrayUtils.EMPTY_STRING_ARRAY);
		res.add(ArrayUtils.EMPTY_STRING_ARRAY);
		return res;
	}

	@RequestMapping("/acn/search")
	public String findAccession(ModelMap model, @RequestParam(required = false, value = "q") String searchQuery,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		model.addAttribute("q", searchQuery);
		if (!StringUtils.isBlank(searchQuery)) {
			try {
				final Page<?> x = searchService.search(searchQuery, new PageRequest(page - 1, 50));
				model.addAttribute("pagedData", x);
				LOG.info("Searching for: " + searchQuery + " returns " + x.getNumberOfElements());
			} catch (SearchException e) {
				LOG.info("Searching for: " + searchQuery + " failed with error " + e.getMessage());
				LOG.error(e.getMessage(), e);
				model.addAttribute("error", e);
			}
		}

		return "/search/accessions";
	}
}
