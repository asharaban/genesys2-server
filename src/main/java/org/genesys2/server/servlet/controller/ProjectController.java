/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.Locale;

import org.genesys2.server.model.impl.Project;
import org.genesys2.server.service.AccessionListService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.ProjectService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/project")
public class ProjectController extends BaseController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private AccessionListService accessionListService;

	@Autowired
	ContentService contentService;

	@RequestMapping({ "/", "" })
	public String projects() {
		return "redirect:/project/list";
	}

	@RequestMapping("/list")
	public String listProjects(ModelMap modelMap, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {

		Page<Project> projects = projectService.list(new PageRequest(page - 1, 20, new Sort("name")));

		modelMap.addAttribute("pagedData", projects);

		return "/project/index";
	}

	@RequestMapping("/{code}")
	public String viewProject(ModelMap modelMap, @PathVariable(value = "code") String code) {
		Project project = projectService.getProjectByCode(code);
		if (project != null) {
			modelMap.addAttribute("project", project);
			modelMap.addAttribute("accessionLists", accessionListService.getLists(project.getAccessionLists()));
			modelMap.addAttribute("blurp", contentService.getArticle(project, "blurp", getLocale()));
		} else {
			throw new ResourceNotFoundException("No project with code " + code);
		}
		return "/project/view";
	}

	@RequestMapping("/add-project")
	public String addProject(ModelMap modelMap) {
		modelMap.addAttribute("project", new Project());
		return "/project/edit";
	}

	@RequestMapping("/{code}/edit")
	public String editProject(ModelMap modelMap, @PathVariable(value = "code") String code) {
		viewProject(modelMap, code);
		return "/project/edit";
	}

	@RequestMapping("/update-project")
	public String update(ModelMap modelMap, @ModelAttribute("project") Project p, @RequestParam("blurp") String aboutBody,
			@RequestParam(value = "summary", required = false) String summary) {

		_logger.debug("Updating project " + p.getCode());
		Project project = null;

		if (p.getId() != null)
			project = projectService.getProjectById(p.getId());

		if (project == null) {
			project = new Project();
		}

		project.setCode(p.getCode());
		project.setUrl(p.getUrl());
		project.setName(p.getName());
		project.setAccessionLists(p.getAccessionLists());

		projectService.saveProject(project);
		projectService.updateBlurp(project, aboutBody, summary, getLocale());

		return "redirect:/project/" + p.getCode();
	}

	protected Locale getLocale() {
		return LocaleContextHolder.getLocale();
	}
}
