package org.genesys2.server.service;

import java.util.List;

import org.genesys2.server.model.json.UserAccessionList;

public interface UserAccessionListService {

	UserAccessionList getList(Long id);

	void save(UserAccessionList userAccessionList);

	List<UserAccessionList> getAll();

	void delete(UserAccessionList userAccessionList);

	List<UserAccessionList> getMyLists();
}
