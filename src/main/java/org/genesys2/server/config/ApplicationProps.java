/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class provides application properties on JSP pages
 */
@Component("props")
public class ApplicationProps implements InitializingBean {

	@Value("${tileserver.cdn}")
	private String tileserverCdn;

	@Value("${cdn.server}")
	private String cdnServer;

	@Value("${base.url}")
	private String baseUrl;

	public String getTileserverCdn() {
		return tileserverCdn;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	public String getCdnServer() {
		return this.cdnServer;
	}

	public String getBaseUrl() {
		return baseUrl;
	}
}
