/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Active accession records with all possible constraints.
 * 
 * @author mobreza
 *
 */
@Entity
@Table(name = "accession", uniqueConstraints = { @UniqueConstraint(name = "UQ_accession_genus_inst", columnNames = { "instituteId", "taxGenus", "acceNumb" }) }, indexes = { @Index(name = "IX_seqNo", columnList = "seqNo") })
public class Accession extends AccessionData {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4847875217506974494L;

	public static final List<Accession> EMPTY_LIST = Collections.unmodifiableList(new ArrayList<Accession>());

	@Column(name = "storage", nullable = false)
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "accessionstorage", joinColumns = @JoinColumn(name = "accessionId", referencedColumnName = "id"))
	@OrderBy("storage")
	private List<Integer> stoRage = new ArrayList<Integer>();

	public List<Integer> getStoRage() {
		return stoRage;
	}

	public void setStoRage(List<Integer> stoRage) {
		this.stoRage = stoRage;
	}

}
