package org.genesys2.server.model.dataset;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("3")
public class DSValueString extends DSValue<String> {

	@Column(length=500)
	private String vals;

	@Override
	public String getValue() {
		return vals;
	}

	@Override
	public void setValue(String value) {
		this.vals = value;
	}
}
