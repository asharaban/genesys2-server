package org.genesys2.server.persistence.domain.dataset;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSRow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DSRowRepository extends JpaRepository<DSRow, Long>, DSRowCustomRepository {

	@Query("select dsrq.row from DSRowQualifier dsrq where dsrq.datasetQualifier.dataset = ?1 and dsrq.value in ( ?2 )")
	List<DSRow> findByQualifier(DS dataset, Set<Long> ids);

	@Query("select dsrq.row from DSRowQualifierLong dsrq where dsrq.datasetQualifier.dataset = ?1 and dsrq.value = ?2")
	DSRow findByLongQualifier(DS dataset, Long value);

	@Query("select dsrq.value, dsrq.row from DSRowQualifierLong dsrq where dsrq.datasetQualifier.dataset = ?1 and dsrq.value in ( ?2 )")
	List<Object[]> findByLongQualifier(DS ds, Collection<?> qualifiers);

	@Query("select distinct dsrq.row from DSRowQualifier dsrq where dsrq.datasetQualifier.dataset = ?1")
	List<DSRow> findByDataset(DS ds);

	@Modifying
	@Query("delete from DSRow dsr where dsr.dataset = ?1")
	int deleteFor(DS ds);

}
