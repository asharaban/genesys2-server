package org.genesys2.server.persistence.domain.dataset;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSQualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DSQualifierRepository extends JpaRepository<DSQualifier, Long> {

	@Modifying
	@Query("delete from DSQualifier dsq where dsq.dataset = ?1")
	int deleteFor(DS ds);

}
