/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import java.io.IOException;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

@Configuration
public class SpringMailConfig {

	@Value("${mail.host}")
	private String host;

	@Value("${mail.port}")
	private Integer port;

	@Value("${mail.user.name}")
	private String userName;

	@Value("${mail.user.password}")
	private String pwd;

	@Value("${mail.smtp.ssl.enable}")
	private String sslEnable;

	@Value("${mail.smtp.starttls.enable}")
	private String smtpStarttlsEnable;

	@Value("${mail.smtp.auth}")
	private String smtpAuth;

	@Value("${mail.transport.protocol}")
	private String protocol;

	@Bean
	public JavaMailSenderImpl mailSender() {
		final JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(host);
		sender.setPort(port);
		sender.setUsername(userName);
		sender.setPassword(pwd);
		sender.setJavaMailProperties(javaMailProperties());
		return sender;
	}

	@Bean
	public VelocityEngine velocityEngine() throws VelocityException, IOException {
		final VelocityEngineFactoryBean velocityEngineFactoryBean = new VelocityEngineFactoryBean();
		velocityEngineFactoryBean.setVelocityProperties(velocityProperties());
		velocityEngineFactoryBean.afterPropertiesSet();
		VelocityEngine engine = velocityEngineFactoryBean.getObject();
		if (engine == null) {
			throw new RuntimeException("Velocity engine could not be created");
		}
		return engine;
	}

	private Properties javaMailProperties() {
		final Properties properties = new Properties();
		properties.setProperty("mail.smtp.ssl.enable", sslEnable);
		properties.setProperty("mail.smtp.starttls.enable", smtpStarttlsEnable);
		properties.setProperty("mail.smtp.auth", smtpAuth);
		properties.setProperty("mail.transport.protocol", protocol);
		return properties;
	}

	private Properties velocityProperties() {
		final Properties properties = new Properties();
		properties.setProperty("resource.loader", "class");
		properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		properties.setProperty("velocimacro.permissions.allow.inline.local.scope", "false");
		properties.setProperty("runtime.log.logsystem.log4j.logger", "velocity");
		properties.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.Log4JLogChute");
		return properties;
	}

}
