/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class AddStuffInterceptor implements HandlerInterceptor {

	// @Autowired
	// private RequestTracker requestTracker;

	@Value("${build.name}")
	private String buildName;

	@Value("${build.revision}")
	private String buildRevision;

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView modelAndView) throws Exception {
		final long startTime = (Long) arg0.getAttribute("springStartTime");
		final long endTime = System.currentTimeMillis();
		final long executeTime = endTime - startTime;
		arg0.setAttribute("springExecuteTime", executeTime);

		try {
			// arg0.setAttribute("lastGet", requestTracker.getLastGet());
		} catch (BeanCreationException e) {
			// No requestTracker bean
		}
	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		final long startTime = System.currentTimeMillis();
		arg0.setAttribute("springStartTime", startTime);
		arg0.setAttribute("buildName", buildName);
		arg0.setAttribute("buildRevision", buildRevision);

		return true;
	}

}
