<%@ tag description="Share on LinkedIn!" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="text" required="false" type="java.lang.String" description="A text parameter appears pre-selected in a Tweet composer. The Tweet author may easily remove the text with a single delete action." %>
<%@ attribute name="summary" required="false" type="java.lang.String" description="The url-encoded description that you wish you use" %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="regular or large" %>
<%@ attribute name="iconOnly" required="false" type="java.lang.Boolean" description="Render only Twitter icon, no text?" %>

<%--
  https://developer.linkedin.com/docs/share-on-linkedin
--%>

<c:url var="refUrl" value="${props.baseUrl}${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}" />

<c:url var="shareUrl" value="https://www.linkedin.com/shareArticle">
  <c:param name="mini" value="true" />
  <c:param name="source" value="GenesysPGR" />
  <c:param name="url" value="${refUrl}" />
  <c:if test="${text != null and text ne ''}">
    <c:param name="title" value="${jspHelper.htmlToText(text, 200)}" />
  </c:if>
  <c:if test="${summary != null and summary ne ''}">
    <c:param name="summary" value="${jspHelper.htmlToText(summary, 256)}" />
  </c:if>
</c:url>
<a class="linkedin-share-button" target="_blank" href="${shareUrl}" data-size="${dataSize}" title="<spring:message code="linkedin.share-this" />">
	<i class="fa fa-linkedin"></i>
	<c:if test="${iconOnly eq null or not iconOnly}">
		<spring:message code="linkedin.share-this" />
	</c:if>
</a>
