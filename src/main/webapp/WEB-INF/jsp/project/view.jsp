<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><c:out value="${project.code} - ${project.name}" /></title>
<meta name="description" content="<c:out value="${jspHelper.htmlToText(blurp.summary)}" />" />
</head>
<body>
	<h1>
		${project.name}
		<small>
			<c:out value="${project.code}" />
		</small>
	</h1>

	<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#crop, 'ADMINISTRATION')">
		<a href="<c:url value="/acl/${project.getClass().name}/${project.id}/permissions"><c:param name="back"><c:url value="/project/${project.code}" /></c:param></c:url>" class="close">
			<spring:message code="edit-acl" />
		</a>
	</security:authorize>
	<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#crop, 'ADMINISTRATION')">
		<a href="<c:url value="/project/${project.code}/edit" />" class="close">
			<spring:message code="edit" />
		</a>
	</security:authorize>

	<c:if test="${blurp ne null}">
		<%@include file="/WEB-INF/jsp/content/include/blurp-display.jsp"%>
	</c:if>

	<div class="content-section-2015">
		<h3>
			<span>
				<spring:message code="project.accessionLists" />
			</span>
		</h3>
		<div class="row">
			<div class="col-md-offset-2 col-md-10">
				<ul>
					<c:forEach items="${accessionLists}" var="accessionList">
						<li>
							<a href="<c:url value="/explore"><c:param name="filter" value='{"lists":["${accessionList.uuid}"]}' /></c:url>"><c:out value="${accessionList.title}" /></a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</body>
</html>
