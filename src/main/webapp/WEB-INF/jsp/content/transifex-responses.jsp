<%@include file="/WEB-INF/jsp/init.jsp" %>

<c:if test="${responseFromTransifex ne null}">
	<div class="alert alert-warning">
		<c:forEach items="${responseFromTransifex}" var="rpt">
			<div>
				<spring:message code="${rpt}" />
			</div>
		</c:forEach>
	</div>
</c:if>